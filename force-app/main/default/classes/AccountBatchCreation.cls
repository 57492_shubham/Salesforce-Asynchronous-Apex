/**
 * @description       : 
 * @author            : Shubham Jaurat
 * @group             : 
 * @last modified on  : 07-09-2024
 * @last modified by  : Shubham Jaurat
**/
public class AccountBatchCreation implements Database.Batchable<sObject> {
    
    public Integer batchSize;
    public Integer totalRecords;
   
    public AccountBatchCreation(Integer totalRecords, Integer batchSize) {
        this.totalRecords = totalRecords;
        this.batchSize = batchSize;
    }
    
    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator('SELECT Id FROM Account LIMIT 1');
    }
    
    public void execute(Database.BatchableContext bc, List<sObject> scope){
        List<Account> lstAcc = new List<Account>();
        for(Integer i = 0; i < batchSize; i++){
            Account objAcc = new Account();
            objAcc.Name = 'Account ' + (i + 1 + (this.totalRecords - batchSize));
            objAcc.NumberOfEmployees = (i + 1 + (this.totalRecords - batchSize));
            lstAcc.add(objAcc);
        }
        insert lstAcc;
    }
    
    public void finish(Database.BatchableContext bc){
        if (this.totalRecords > this.batchSize) {
            Integer remainingRecords = this.totalRecords - this.batchSize;
            Integer nextBatchSize = (remainingRecords > this.batchSize) ? this.batchSize : remainingRecords;
            AccountBatchCreation nextBatch = new AccountBatchCreation(remainingRecords, this.batchSize);
            Database.executeBatch(nextBatch, nextBatchSize);
        }
    }
}




// Run below code in Ananymous Window:

// AccountBatchCreation batch = new AccountBatchCreation(50000, 200);
// Database.executeBatch(batch, 200);