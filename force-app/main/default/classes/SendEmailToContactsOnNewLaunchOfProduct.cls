/**
 * @description       : 
 * @author            : Shubham Jaurat
 * @group             : 
 * @last modified on  : 09-21-2024
 * @last modified by  : Shubham Jaurat
**/
public class SendEmailToContactsOnNewLaunchOfProduct implements Database.Batchable<SObject> {

    // Start method to define the scope of the query
    public Database.QueryLocator start(Database.BatchableContext bc) {
        // Query all contacts associated with accounts in the "Technology" industry
        return Database.getQueryLocator([
            SELECT Id, FirstName, LastName, Email, Account.Name
            FROM Contact
            WHERE Account.Industry = 'Technology' AND Email != null
        ]);
    }

    // Execute method to process each batch of records
    public void execute(Database.BatchableContext bc, List<Contact> contactList) {
        // List to hold email messages
        List<Messaging.SingleEmailMessage> emailsToSend = new List<Messaging.SingleEmailMessage>();
        System.debug('In Execute'+con.Email);
        // Iterate over each contact and create a personalized email
        for (Contact con : contactList) {
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            System.debug('con.Email'+con.Email);
            email.setToAddresses(new String[] { con.Email });
            email.setSubject('Exciting New Product Launch!');
            
            // Create personalized email body
            String body = 'Dear ' + con.FirstName + ',\n\n';
            body += 'We are excited to introduce our new product, which we believe will be a great fit for your company, ' + con.Account.Name + '.\n\n';
            body += 'Best regards,\nYour Company Name';
            email.setPlainTextBody(body);
            
            emailsToSend.add(email); // Add email to list
        }
        
        // Send the emails in bulk
        if (!emailsToSend.isEmpty()) {
            System.debug('emailsToSend');
            Messaging.sendEmail(emailsToSend);
        }
    }

    // Finish method for post-batch actions
    public void finish(Database.BatchableContext bc) {
        // Optional: Send a summary email or log the batch processing
        System.debug('Mass email processing completed!');
    }
}


// SendEmailToContactsOnNewLaunchOfProduct emailBatch = new SendEmailToContactsOnNewLaunchOfProduct();
// Database.executeBatch(emailBatch, 100);